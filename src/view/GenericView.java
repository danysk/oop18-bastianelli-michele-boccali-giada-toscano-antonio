package view;

import controller.menu.Controller;
import javafx.geometry.Dimension2D;
import javafx.scene.Scene;

/**
 * GenericView interface.
 *
 */
public interface GenericView {


    /**
     * Returns the scene of the generic view with an AnchorPane as root.
     * 
     * @return the scene
     */
    Scene getScene();

    /**
     * Returns the dimension of the window.
     * 
     * @return the dimension of the window
     */
    Dimension2D getDimension();

    /**
     * Return the controller. It must be call to implements communication between
     * view and controller.
     * 
     * @return view's controller
     */
    Controller getController();

}
