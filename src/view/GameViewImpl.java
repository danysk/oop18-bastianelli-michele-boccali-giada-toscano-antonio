package view;

import common.CommonStrings;
import controller.GameControllerImpl;
import controller.menu.Controller;
import enumerators.Level;
import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.transform.Scale;

/**
 * View of the game.
 */
public final class GameViewImpl extends ScoreView implements GameView {

    /**
     * A second in nanoseconds.
     */
    public static final long ONE_SECOND_NS = 1000000000;
    private static final int MAX_UPDATES = 8;
    private final long fps;
    private long lastTime = System.nanoTime();

    private boolean endGame;
    private Camera camera;

    private final AnimationTimer animationTimer = new AnimationTimer() {

        @Override
        public void handle(final long time) {
            render(time);
        }
    };

    /**
     * View constructor. Create camera
     * 
     * @param c   the controller
     * @param fps - game fps
     */
    public GameViewImpl(final long fps, final Controller c) {
        super(c);
        this.fps = fps;

        super.init(super.getHBox());
    }

    /* (non-Javadoc)
     * @see view.GameView#init(enumerators.Level)
     */
    @Override
    public void init(final Level level) {

        super.getAnchorPane().setStyle(" -fx-background-image : url(" + level.getBackgroundPath() + ");");

        final Scale scale = new Scale(CommonStrings.SCALING_FACTOR, CommonStrings.SCALING_FACTOR);
        scale.setPivotX(0);
        scale.setPivotY(0);
        getScene().getRoot().getTransforms().setAll(scale);
        this.camera = new Camera();
    }

    /* (non-Javadoc)
     * @see view.GameView#getViewNodes()
     */
    @Override
    public ObservableList<Node> getViewNodes() {
        return super.getAnchorPane().getChildren();
    }

    /* (non-Javadoc)
     * @see view.GameView#getViewDimension()
     */
    @Override
    public Dimension2D getViewDimension() {
        return super.getDimension();
    }

    /* (non-Javadoc)
     * @see view.GameView#getCamera()
     */
    @Override
    public Camera getCamera() {
        return camera;
    }

    /* (non-Javadoc)
     * @see view.GameView#timerStart()
     */
    @Override
    public void timerStart() {
        endGame = false;
        animationTimer.start();
    }

    /* (non-Javadoc)
     * @see view.GameView#timerStop()
     */
    @Override
    public void timerStop() {
        endGame = true;
        animationTimer.stop();
    }

    /* (non-Javadoc)
     * @see view.GameView#getCameraPosition()
     */
    @Override
    public Point2D getCameraPosition() {
        return camera.getCameraPosition();
    }

    /**
     * Renders an update. Used to fix FPS on different platforms.
     * 
     * @param time - execution time
     */
    public void render(final long time) {
        long timeDelta = time - lastTime;
        lastTime = time;

        GameControllerImpl.getInstance().tick();
        int updateCount = 0;
        while (timeDelta > 0 && (MAX_UPDATES <= 0 || updateCount < MAX_UPDATES && !endGame)) {
            final long updateTimeStep = Math.min(timeDelta, ONE_SECOND_NS / fps);

            GameControllerImpl.getInstance().tick();
            timeDelta -= updateTimeStep;
            updateCount++;
        }
        final long sleepTime = (ONE_SECOND_NS / fps) - (System.nanoTime() - lastTime);
        if (sleepTime <= 0) {
            return;
        }
        final long prevTime = System.nanoTime();
        while (System.nanoTime() - prevTime <= sleepTime) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
