package view.menu;

import javafx.scene.control.Button;
import view.GenericView;

/**
 * This class create an abstract AbstractEventBusButton that send a MsgEvent on
 * the given view controller when is pressed. The abstract method setAction must
 * be implemented to do something before event is post to the bus.
 */
public abstract class AbstractEventButton {

    private final Button b;
    private final String msgString;

    /**
     * AbstractEventButton constructor. It set button message and set button action.
     * 
     * @param view      : view who create the button
     * @param msgString : message to be send
     */
    public AbstractEventButton(final GenericView view, final String msgString) {
        this.msgString = msgString;
        b = new Button();
        b.setOnAction(e -> {
            this.setAction();
            view.getController().sendMsg(this.getMsg());
        });
    }

    /**
     * Action to do on button pression. It is called before sending the button
     * message to the controller
     */
    public abstract void setAction();

    /**
     * Return the button intance.
     * 
     * @return the button
     */
    public Button getButton() {
        return b;
    }

    /**
     * Get the button message.
     * 
     * @return button message
     */
    public String getMsg() {
        return msgString;
    }
}
