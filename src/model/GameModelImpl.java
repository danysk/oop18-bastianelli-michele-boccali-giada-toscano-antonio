package model;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import com.google.common.collect.Maps;

import controller.GameControllerImpl;
import controller.entities.Coin;
import controller.entities.Enemy;
import controller.entities.Entity;
import controller.entities.Platform;
import controller.entities.Player;
import enumerators.Level;
import enumerators.PlayerCharacter;
import factories.AbstractFactory;
import utils.Box2DUtils;

/**
 * Contains all the world entities and the JBox2D world and bodies. It handles
 * the world steps.
 */
public final class GameModelImpl implements GameModel {

    private static final int VELOCITY_ITERATION = 10;
    private static final int POSITION_ITERATIONS = 3;
    private final float fps;

    private static final Vec2 GRAVITY = new Vec2(0, 10f);

    private static final World WORLD = new World(GRAVITY);
    private final Map<Body, Entity> entitiesMap;
    private final Map<Body, Entity> toBeDeletedMap;
    private Player player;
    private Platform topPlatform;
    private boolean playerDead;
    private double maxHeight;



    /**
     * Instantiates the game entities and the JBox2D world.
     * 
     * @param fps - the framerate the game should update
     */
    public GameModelImpl(final long fps) {
        this.fps = (float) fps;
        WORLD.setContactListener(new CollisionHandler(this));
        entitiesMap = Maps.newConcurrentMap();
        toBeDeletedMap = Maps.newConcurrentMap();
    }

    @Override
    public void init(final Level level) {
        WORLD.setGravity(GRAVITY.mul((float) level.getDifficulty()));
    }

    @Override
    public void updateWorld() {
        GameModelImpl.WORLD.step(1.0f / fps, VELOCITY_ITERATION, POSITION_ITERATIONS);
        if (Box2DUtils.box2DYToJavaFX(Math.round(player.getPhysicPosition().y)) > maxHeight) {
            maxHeight = Box2DUtils.box2DYToJavaFX(player.getPhysicPosition().y);
        }
    }


    @Override
    public void updateEntities() {
        if (!entitiesMap.isEmpty()) {
            entitiesMap.values().stream().forEach(Entity::updateEntity);
        }
    }

    /**
     * Returns the JBox2D world.
     * 
     * @return the JBox2D world.
     */
    public static World getWorld() {
        return WORLD;
    }

    @Override
    public Set<Enemy> getEnemiesSet() {
        return entitiesMap.values().stream().filter(en -> en instanceof Enemy).map(en -> (Enemy) en)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Coin> getCoinSet() {
        return entitiesMap.values().stream().filter(co -> co instanceof Coin).map(co -> (Coin) co)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Platform> getPlatformSet() {
        return entitiesMap.values().stream().filter(pl -> pl instanceof Platform).map(pl -> (Platform) pl)
                .collect(Collectors.toSet());
    }

    @Override
    public Platform getTopPlatform() {
        return topPlatform;
    }

    @Override
    public void setTopPlatform(final Platform topPlatform) {
        this.topPlatform = topPlatform;
    }

    @Override
    public Set<Entity> getAllEntities() {
        return entitiesMap.values().stream().filter(ent -> ent instanceof Entity).map(ent -> (Entity) ent)
                .collect(Collectors.toSet());
    }

    @Override
    public void removeEntityFromMap(final Entity entity) {
        if (!entitiesMap.isEmpty() && entitiesMap.containsKey(entity.getBody())) {
            toBeDeletedMap.put(entity.getBody(), entity);
        }
    }

    @Override
    public void addEntityToMap(final Entity entity, final Body body) {
        entitiesMap.put(body, entity);
    }

    @Override
    public Entity getEntityFromBody(final Body body) {
        if (!entitiesMap.isEmpty()) {
            if (entitiesMap.containsKey(body)) {
                return entitiesMap.get(body);
            }
            throw new IllegalArgumentException("This body doesn't exist inside the entity collection!");
        } else {
            throw new IllegalArgumentException("Entity collection is empty");
        }
    }

    @Override
    public void addEntityToDestroy(final Entity entity) {
        toBeDeletedMap.put(entity.getBody(), entity);
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean isPlayerDead() {
        return playerDead;
    }

    @Override
    public void createPlayer(final PlayerCharacter character, final Vec2 position) {
        player = AbstractFactory.createPlayer(character, position);
        addEntityToMap(player, player.getBody());
        this.maxHeight = Box2DUtils.box2DYToJavaFX(position.y);
        this.playerDead = false;

    }

    @Override
    public void checkEntitiesToDelete() {
        entitiesMap.values()
                    .stream()
                    .filter(e -> (!e.getModel().isAlive() || GameControllerImpl.getInstance().getGameView().getCamera().isYPastPlayer(e.getPhysicPosition().y)))
                    .forEach(e -> {
                        e.destroy();
                        toBeDeletedMap.put(e.getBody(), e);
                    });
        if (toBeDeletedMap.containsValue(player)) {
            playerDead = true;
        }
        deleteEntities();
    }

    @Override
    public void cleanAllEntities() {
        entitiesMap.values()
                    .stream()
                    .forEach(e -> {
                        e.destroy();
                        toBeDeletedMap.put(e.getBody(), e);
                    });
        deleteEntities();
    }

    @Override
    public void deleteEntities() {
        if (!WORLD.isLocked() && !toBeDeletedMap.isEmpty()) {
            toBeDeletedMap.keySet().stream().forEach(b -> {
                WORLD.destroyBody(b);
                if (entitiesMap.containsKey(b)) {
                    entitiesMap.remove(b);
                }
            });
            toBeDeletedMap.clear();
        }
    }

    @Override
    public void resetWorld() {
        this.cleanAllEntities();
    }

    @Override
    public double getMaxHeight() {
        return maxHeight;
    }
}
