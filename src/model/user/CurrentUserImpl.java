package model.user;

import java.util.Optional;

import javax.crypto.Cipher;

import common.CommonStrings;
import enumerators.Level;
import utils.CryptDecryptString;

/**
 * Class that describe the User currently connected in the application. This
 * class is singleton.
 */

public final class CurrentUserImpl implements UserModel {

    private static final UserImpl HOST_ACCOUNT = new UserImpl(new UserProfileImpl("host",
            CryptDecryptString.execCryptDecrypt(Cipher.ENCRYPT_MODE, CommonStrings.KEY, "host")));

    private static CurrentUserImpl instance = new CurrentUserImpl();
    private UserImpl user;
    private Level currentLevel;

    private CurrentUserImpl() {
        user = HOST_ACCOUNT;
    }

    /**
     * get the singleton instance of the current user.
     * @return the singleton instance of the current user
     */
    public static CurrentUserImpl getInstance() {
        return instance;
    }

    @Override
    public UserImpl getUser() {
        return user;
    }

    @Override
    public void setUser(final UserImpl user) {
        this.user = user;
    }

    @Override
    public Optional<Level> getCurrentLevel() {
        return Optional.ofNullable(currentLevel);
    }

    @Override
    public void setCurrentLevel(final Level currentLevel) {
        this.currentLevel = currentLevel;
    }

    @Override
    public void setNextCurrentLevel() {
        Optional.ofNullable(currentLevel).ifPresent(e -> {
            try {
                this.setCurrentLevel(Level.values()[e.ordinal() + 1]);
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("You are already in the max game Level");
            }
        });
    }
}
