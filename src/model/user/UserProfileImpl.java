package model.user;

import java.io.Serializable;

/**
 * User profile information to login. It contains username and password.
 */
public class UserProfileImpl implements Serializable, UserProfile {

    private static final transient long serialVersionUID = 7059418592537089664L;

    private static final String USERNAME = "Username: ";
    private static final String PASSWORD = "Password: ";

    private String username;
    private String password;

    /**
     * Contructor with user profile information.
     * @param username : the username
     * @param password : the password
     */
    public UserProfileImpl(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public final String getUsername() {
        return username;
    }

    @Override
    public final void setUsername(final String username) {
        this.username = username;
    }

    @Override
    public final String getPassword() {
        return password;
    }

    @Override
    public final void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public final String toString() {
        return usernameToString() + ", " + passwordToString() + ", ";
    }

    @Override
    public final String usernameToString() {
        return USERNAME + username;
    }

    @Override
    public final String passwordToString() {
        return PASSWORD + password;
    }
}
