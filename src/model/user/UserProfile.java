package model.user;

/**
 * User profile interface.
 */
public interface UserProfile {

    /**
     * Get user's username.
     * @return user's username
     */
    String getUsername();

    /**
     * Set user's username.
     * @param username : user's username
     */
    void setUsername(String username);

    /**
     * Get user's password.
     * @return the password
     */
    String getPassword();

    /**
     * Set user's password.
     * @param password : the password
     */
    void setPassword(String password);

    /**
     * username's to string.
     * @return username's to string
     */
    String usernameToString();

    /**
     * password's to string.
     * @return username's to string
     */
    String passwordToString();

}
