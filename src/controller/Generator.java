package controller;

import enumerators.Level;
import model.GameModel;

/**
 * Interface for a entity generator.
 */
public interface Generator {

    /**
     * Initializes the generator.
     * 
     * @param level - the level to generate
     * @param model - model of the game
     */
    void init(Level level, GameModel model);

    /**
     * Does an update in the generator, check if new entities are needed.
     */
    void update();
}
