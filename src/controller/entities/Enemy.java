package controller.entities;

import common.events.CollisionEvent;
import model.entities.EnemyModel;
import view.entities.EntityView;

/**
 * Enemy controller.
 */
public class Enemy extends AbstractEntity<EnemyModel> {

    /**
     * Create a enemy.
     * @param model the entity model
     * @param view the entity view
     */
    public Enemy(final EnemyModel model, final EntityView view) {
        super(model, view);
    }

    @Override
    protected void handleCollisionEvent(final CollisionEvent collisionEvent) {
        // TODO Auto-generated method stub
    }
}
