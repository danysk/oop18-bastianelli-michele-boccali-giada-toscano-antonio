package controller.menu;

import view.GenericView;

/**
 * Generic controller. It must control a view
 */
public interface Controller {

    /**
     * Send a String to the controller. This message are used to implements
     * communication between view and controller
     * 
     * @param msg : message to be handled
     */
    void sendMsg(String msg);

    /**
     * @return the view
     */
    GenericView getView();
}
