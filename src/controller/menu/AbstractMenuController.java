package controller.menu;

import common.SceneEventBusConnection;
import model.user.UserModel;
import model.user.UserModelImpl;

/**
 * Create an abstract controller connected on the main EventBus.
 * This controller extends SceneEventBusConnection
 */
public abstract class AbstractMenuController extends SceneEventBusConnection implements Controller {

    private final UserModel model;

    /**
     * ControllerImpl constructor. Call SceneEventBusConnection constructor
     */
    protected AbstractMenuController() {
        super();
        model = new UserModelImpl();
    }

    /**
     * Get the UserModel instance.
     * @return the UserModel instance
     */
    public UserModel getModel() {
        return model;
    }
}
