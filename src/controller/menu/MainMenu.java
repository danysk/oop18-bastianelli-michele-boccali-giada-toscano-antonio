package controller.menu;

import java.util.Arrays;

import common.MsgStrings;
import enumerators.Level;
import enumerators.SceneType;
import view.BackgroundMusic;
import view.GenericView;
import view.menu.MainMenuView;

/**
 * The main menu controller class. This class create the MainMenuView.
 */
public class MainMenu extends AbstractMenuController {

    private final MainMenuView view;

    /**
     * MainMenu constructor.
     */
    public MainMenu() {
        super();
        this.view = new MainMenuView(this, getModel().getUser().getMaxLevel().ordinal());
        this.view.setTitle(getModel().getUser().getUserProfile().getUsername());
        this.view.setInfo(getModel().getUser().getCoin(), getModel().getUser().getMaxHeight());
    }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.SWITCH_AUDIO_MODE:
            BackgroundMusic.getInstance().switchAudioMode();
            break;
        case MsgStrings.STATISTICS:
            this.postSceneAndUnregister(SceneType.STATISTICS);
            break;
        case MsgStrings.SHOP:
            this.postSceneAndUnregister(SceneType.SHOP);
            break;
        default:
            Arrays.asList(Level.values()).forEach(e -> {
                if (e.toString().equals(msg)) {
                    getModel().setCurrentLevel(e);
                    this.postSceneAndUnregister(SceneType.NEW_GAME);
                }
            });
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

}
