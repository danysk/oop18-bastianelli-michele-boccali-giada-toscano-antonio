package common;

import common.events.SceneEvent;
import enumerators.SceneType;

/**
 * Class to post SceneEvent. Extends EventBusConnection.
 *
 */
public class SceneEventBusConnection extends EventBusConnection {

    /**
     * Post a SceneEvent on the bus, then unregister the class from the EventBus.
     * 
     * @param scene : the sceneType to post
     */
    public void postSceneAndUnregister(final SceneType scene) {
        this.postScene(scene);
        this.unregister();
    }

    /**
     * Post a SceneEvent on the bus.
     * 
     * @param scene : the sceneType to post
     */
    public void postScene(final SceneType scene) {
        this.getBus().post(new SceneEvent(scene));
    }
}
