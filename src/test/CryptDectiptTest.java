package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import javax.crypto.Cipher;

import org.junit.Test;

import utils.CryptDecryptString;

/**
 *
 */
public class CryptDectiptTest {
    /**
     * 
     */
    @Test
    public void testCryptDecript() {
        final String dectiptString = "test";
        String encriptString = CryptDecryptString.execCryptDecrypt(Cipher.ENCRYPT_MODE, "Cript_decript_ok", dectiptString);
        assertNotEquals("Crypt equals to encrypt", encriptString, dectiptString);
        encriptString = CryptDecryptString.execCryptDecrypt(Cipher.DECRYPT_MODE, "Cript_decript_ok", encriptString);
        assertEquals("Crypt equals to encrypt", encriptString, dectiptString);
    }
}
