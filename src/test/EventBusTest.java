package test;

import static org.junit.Assert.assertSame;

import org.junit.Test;

import common.SceneEventBusConnection;
import controller.GameControllerImpl;
import controller.menu.MainControllerImpl;

/**
 * Test the correct event bus connection.
 */
public class EventBusTest {

    /**
     * Check the correct registration of the MainController.
     */
    @Test
    public void checkRegistration() {
        final SceneEventBusConnection c = new MainControllerImpl(null);
        assertSame("Not the same bus", GameControllerImpl.getInstance().getBus(), 
                c.getBus());
    }
}
