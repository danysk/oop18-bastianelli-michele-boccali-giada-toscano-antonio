package test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import common.CommonStrings;
import controller.GameControllerImpl;
import controller.entities.Coin;
import controller.entities.Enemy;
import enumerators.Level;
import enumerators.PlayerCharacter;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.stage.Stage;
import main.JumpManiaApp;


/**
 *
 */
public class GeneratorTest {

    /**
     * 
     * @throws java.lang.Exception - exception
     */
    @Before
    public void setUp() throws Exception {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                new JFXPanel(); // Initializes the JavaFx Platform
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new JumpManiaApp().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        } 
                    }
                });
            }
        });
        thread.start();
        Thread.setDefaultUncaughtExceptionHandler(GeneratorTest::handleOldJavaFXThread);
        GameControllerImpl.getInstance().initNewGame(PlayerCharacter.BIRD, Optional.of(Level.LEVEL_1));

    }

    /**
     * Tests if the GameController and the GameModel are not null.
     */
    @Test
    public void testNotNull() {
        assertNotNull("GameController is null", GameControllerImpl.getInstance());
        assertNotNull("GameModel is null", GameControllerImpl.getInstance().getGameModel());
    }

    /**
     * Tests if the generated entities aren't out of bound.
     */
    @Test
    public void testScreenPosition() {
        final Iterator<controller.entities.Platform> pIterator = GameControllerImpl.getInstance().getGameModel().getPlatformSet().iterator();
        while (pIterator.hasNext()) {
            final controller.entities.Platform platform = pIterator.next();
            assertTrue("Platform is outside", platform.getViewPosition().x >= 0);
            assertTrue("Platform is outside", platform.getViewPosition().x < CommonStrings.WINDOW_WIDTH);
        }
    }

    /**
     * Tests if enemies are generated on the same platform as a coin or the other way around.
     */
    @Test
    public void testGenerationOnSamePlatform() {
        final Iterator<Enemy> eIterator = GameControllerImpl.getInstance().getGameModel().getEnemiesSet().iterator();
        while (eIterator.hasNext()) {
            final Enemy enemy = eIterator.next();
            final Iterator<Coin> cIterator = GameControllerImpl.getInstance().getGameModel().getCoinSet().iterator();
            while (cIterator.hasNext()) {
                final Coin coin = cIterator.next();
                assertNotEquals("Two entities on the same platform", coin.getPhysicPosition().x, enemy.getPhysicPosition().x);
            }
        }
    }

    private static void handleOldJavaFXThread(final Thread t, final Throwable throwable) {
        //empty method to ignore the exception for the old JavaFX thread not yet terminated.
    }
}
