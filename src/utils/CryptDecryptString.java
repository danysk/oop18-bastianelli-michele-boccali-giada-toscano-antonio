package utils;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * The CryptDecryptString class crypt or decript string.
 */
public final class CryptDecryptString {

    private CryptDecryptString() {
    }

    /**
     * Method to crypt or decript a string.
     * 
     * @param cipherMode : used to initialize Chiper Mode.
     * @param key        : the key material of the secret key
     * @param input      : the string to be crypted/decrypted
     * @return the crypted/decrypted string
     */
    public static String execCryptDecrypt(final int cipherMode, final String key, final String input) {
        try {
            final Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, secretKey);

            final byte[] outputBytes = cipher.doFinal(input.getBytes());

            return new String(outputBytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
