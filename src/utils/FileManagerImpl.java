package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * File Manager class to handle file. It handle Set of T object.
 *
 * @param <T> generic class
 */
public class FileManagerImpl<T> implements FileManager<T> {

    @SuppressWarnings({ "unchecked", "resource" })
    @Override
    public final Set<T> load(final String fileName) {
        Set<T> set = new HashSet<>();
        try {
            final File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);
            final ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                set = set.getClass().cast(ois.readObject());
            } catch (ClassCastException e1) {
                file.delete();
                fis = new FileInputStream(fileName);
                System.out.println("ClassCastException" + e1.getMessage());
            }
            ois.close();
            return set;
        } catch (FileNotFoundException e2) {
            System.out.println("FileNotFoundException" + e2.getMessage());
        } catch (IOException e3) {
            System.out.println("IOException" + e3.getMessage());
        } catch (ClassNotFoundException e4) {
            System.out.println("ClassNotFoundException" + e4.getMessage());
        }

        return set;
    }

    @Override
    public final void save(final String fileName, final Set<T> set) {
        try {
            final FileOutputStream fos = new FileOutputStream(fileName);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(set);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
